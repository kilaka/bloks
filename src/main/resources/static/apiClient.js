

function ApiClient(url){
    this.url = url;

    this.getTime = function(callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url + "/api/getTime", true);
        xhr.onreadystatechange = (function() {
                                     return function() {
                                        if (xhr.readyState == 4 && xhr.status == 200) {
                                            var res = xhr.responseText;
                                            callback(res);
                                        }
                                     }
                                 })();
        xhr.send();
    }

}


