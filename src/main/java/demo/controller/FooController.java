package demo.controller;

import org.springframework.stereotype.Controller;

import demo.api.Api;

import java.util.Date;

@Controller
public class FooController implements Api {

	@Override
	public String getTime() {
		return "Time on server: " + new Date();
	}

}
