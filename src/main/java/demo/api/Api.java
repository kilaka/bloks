package demo.api;

import org.springframework.web.bind.annotation.*;

@RequestMapping(path = "/api")
public interface Api {

	@RequestMapping(path="/getTime", method= RequestMethod.GET)
	@ResponseBody
	String getTime();
}
