package demo.client;

import org.springframework.web.client.RestTemplate;

import demo.api.Api;

public class ApiClient implements Api {

    private final String url;

    public ApiClient(String url) {
        this.url = url;
    }

	@Override
	public String getTime() {
		RestTemplate restTemplate = new RestTemplate();
		String res = restTemplate.getForObject(url + "/api/getTime", String.class);
		return res;
	}

}
