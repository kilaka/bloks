package demo.Utils;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class TemplateHelper {

    public enum TemplateType
    {
        Java,
        JavaScript
    }
    public static String ReadTemplateFile(TemplateType templateType) throws IOException {
        String templateName;
        switch  (templateType)
        {
            case JavaScript:
                templateName = "js.template";
                break;
            case Java:
            default:
                templateName =  "java.template";
        }

        InputStream inputStream = TemplateHelper.class.getClassLoader().getResourceAsStream(templateName);

        return CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));
    }

    public static void WriteStringToFile(String text, String filePath) throws IOException {
        Files.write(Paths.get(filePath), text.getBytes());
    }

    public static String FillTemplate(String input, Map<String,String> attributes) {
        String str = input;
        for (Map.Entry<String, String> attribute : attributes.entrySet()) {
            str = str.replace(attribute.getKey(), attribute.getValue());
        }
        return str;
    }
}
