package demo.Utils;

import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class TemplateHelperTest {
    @Test
    public void FillTemplateTest()
    {
        String str = new String("AAA $(atr1) BBB$(atr2)CCC $(atr1)");
        String expected = new String("AAA <test1> BBB<test2>CCC <test1>");

        HashMap<String,String> attributes = new HashMap<>();
        attributes.put("$(atr1)","<test1>");
        attributes.put("$(atr2)","<test2>");

        String output = TemplateHelper.FillTemplate(str, attributes);
        assertEquals(expected,output);
    }

    @Test
    public void ReadTemplateFileTest()
    {
        String expected = "package demo.client;\n" +
                "\n" +
                "import org.springframework.web.client.RestTemplate;\n" +
                "\n" +
                "import demo.api.Api;\n" +
                "\n" +
                "import java.util.HashMap;\n" +
                "import java.util.Map;\n" +
                "\n" +
                "public class ${serviceName} implements ${serviceInterface} {\n" +
                "\n" +
                "    private final String url;\n" +
                "\n" +
                "    public ${serviceName}(String url) {\n" +
                "        this.url = url\n" +
                "    }\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic ${retType} ${methodName}() {\n" +
                "\t\tRestTemplate restTemplate = new RestTemplate();\n" +
                "\t\t${retType} res = restTemplate.getForObject(url + \"{path}\", ${retType}.class);\n" +
                "\t\treturn res;\n" +
                "\t}\n" +
                "\n" +
                "}\n";

        String output;
        try {
            output = TemplateHelper.ReadTemplateFile(TemplateHelper.TemplateType.Java);
            System.out.println(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}