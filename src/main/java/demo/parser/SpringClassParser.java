package demo.parser;

import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class SpringClassParser implements IClassParser {

    private final String SERVICE_NAME = "${serviceName}";
    private final String SERVICE_PATH = "${servicePath}";
    private final String RETURN_TYPE = "${retType}";
    private final String METHOD_NAME = "${methodName}";
    private final String METHOD_PATH = "${methodPath}";
    private final String HTTP_METHOD = "${httpMethod}";

    public Map<String,String> parseClass(String className) throws ClassNotFoundException {
        Map<String, String> classMap = new HashMap<>();
        // get class object from class name
        Class<?> cls = Class.forName(className);
        // class name
        classMap.put(SERVICE_NAME, cls.getSimpleName());
        // class request mapping attribues
        RequestMapping classRequestMapping = cls.getAnnotation(RequestMapping.class);
        if (classRequestMapping == null || classRequestMapping.path().length < 1) {
            System.out.println("parseClass - Failed to request mapping annotation or path for class");
            return null;
        }
        classMap.put(SERVICE_PATH, classRequestMapping.path()[0]);
        // get methods
        Method[] methods = cls.getMethods();
        if (methods.length < 1) {
            System.out.println("parseClass - Failed to find any methods for class");
            return null;
        }
        Method firstMethod = methods[0];
        // method name
        classMap.put(METHOD_NAME, firstMethod.getName());
        // method return type
        classMap.put(RETURN_TYPE, firstMethod.getReturnType().getSimpleName());

        // method request mapping attribues
        RequestMapping methodRequestMapping = firstMethod.getAnnotation(RequestMapping.class);
        if (methodRequestMapping == null || methodRequestMapping.path().length < 1) {
            System.out.println("parseClass - Failed to request mapping annotation or path for method");
            return null;
        }

        classMap.put(METHOD_PATH, methodRequestMapping.path()[0]);

        classMap.put(HTTP_METHOD, methodRequestMapping.method()[0].toString());

        return classMap;
    }
}
