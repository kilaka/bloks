package demo.parser;

import java.util.Map;

import org.junit.Test;


import static org.junit.Assert.assertEquals;

public class SpringClientGeneratorTest {

    private final String SERVICE_NAME = "${serviceName}";
    private final String SERVICE_INTERFACE = "${serviceInterface}";
    private final String SERVICE_PATH = "${servicePath}";
    private final String RETURN_TYPE = "${retType}";
    private final String METHOD_NAME = "${methodName}";
    private final String METHOD_PATH = "${methodPath}";
    {}

    @Test
    public void parseClass() {

        SpringClassParser classParser = new SpringClassParser();
        try {
            Map<String, String> classMap = classParser.parseClass("demo.api.Api");
            assertEquals(classMap.get(SERVICE_NAME),"Api");
            assertEquals(classMap.get(SERVICE_PATH),"/api");
            assertEquals(classMap.get(RETURN_TYPE),"String");
            assertEquals(classMap.get(METHOD_PATH),"/getTime");
            assertEquals(classMap.get(METHOD_NAME),"getTime");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}