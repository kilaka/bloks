package demo.parser;

import java.util.Map;

public interface IClassParser {
    Map<String,String> parseClass(String className) throws ClassNotFoundException;
}
