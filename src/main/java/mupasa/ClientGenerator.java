package mupasa;

import demo.Utils.TemplateHelper;
import demo.parser.IClassParser;
import demo.parser.SpringClassParser;

import java.io.IOException;
import java.util.Map;

public class ClientGenerator {

    public static void main(String[] args) {
        IClassParser classParser = new SpringClassParser();
        Map<String,String> classMap;
        try {
            classMap = classParser.parseClass("demo.api.Api");
        } catch (ClassNotFoundException e) {
            System.out.println("Failed to parse class");
            return;
        }
        // Java file
        CreateClientFile(classMap, TemplateHelper.TemplateType.Java, "/home/alik/DevP/bloks/src/main/java/demo/client/ApiClient.java");

        // JavaScript file
        CreateClientFile(classMap, TemplateHelper.TemplateType.JavaScript, "/home/alik/DevP/bloks/src/main/resources/static/apiClient.js");

    }

    private static void CreateClientFile(Map<String, String> classMap, TemplateHelper.TemplateType templateType, String outputFilePath) {
        String inputFileStr;
        try {
            inputFileStr = TemplateHelper.ReadTemplateFile(templateType);
        } catch (IOException e) {
            System.out.println("Failed to read template inputFileStr");
            return;
        }

        String outputFileStr = TemplateHelper.FillTemplate(inputFileStr, classMap);

        try {
            TemplateHelper.WriteStringToFile(outputFileStr, outputFilePath);
        } catch (IOException e) {
            System.out.println("Failed to write to file: " + outputFilePath);
        }
    }
}
